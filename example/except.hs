import           Control.Monad.Claim
import qualified Test.Resource       as R


main :: IO ()
main = runClaimT $ do
  r1 <- R.good 12
  r2 <- R.acquireErr 14
  R.use r1
  R.use r2
