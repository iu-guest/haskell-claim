import           Control.Monad.Claim
import qualified Test.Resource       as R

main :: IO ()
main = runClaimT $ do
  r <- R.good 10
  R.use r
  R.use r
