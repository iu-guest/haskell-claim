import           Control.Monad.Claim
import qualified Test.Resource       as R


main :: IO ()
main = runClaimT $ do
  r1 <- R.good 12
  r2 <- R.releaseErr 14
  R.use r1
  R.use r2
