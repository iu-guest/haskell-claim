import           Control.Monad.Claim
import qualified Test.Resource       as R

main :: IO ()
main = runClaimT $ do
  r1 <- R.good 10
  r2 <- R.good 12
  R.use r2
  R.use r1
