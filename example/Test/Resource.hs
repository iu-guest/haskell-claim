module Test.Resource (
  Resource,
  good,
  acquireErr,
  releaseErr,
  use,
  misuse,
  ResourceError) where
import           Control.Monad.Catch
import           Control.Monad.Claim
import           Control.Monad.IO.Class
import           Text.Printf

-- | Abstract datatype, representing some resource. In our case,
-- acquisition and release are modelled via output on terminal.
newtype Resource s = Resource Int

data ResourceError =
  AcquireFailed
  | ReleaseFailed
  | UseFailed deriving Show
instance Exception ResourceError

goodAcquire :: Int -> IO (Resource s)
goodAcquire n = printf "acquire %d\n" n *> pure (Resource n)

goodRelease :: Resource s -> IO ()
goodRelease (Resource n) = printf "release %d\n" n


badAcquire :: Int -> IO (Resource s)
badAcquire _ = throwM AcquireFailed

badRelease :: Resource s -> IO ()
badRelease _ = throwM ReleaseFailed

good :: Int -> Claim s (Resource s)
good n = claim (goodAcquire n) goodRelease

acquireErr :: Int -> Claim s (Resource s)
acquireErr n = claim (badAcquire n) goodRelease

releaseErr :: Int -> Claim s (Resource s)
releaseErr n = claim (goodAcquire n) badRelease

use :: Resource s -> Claim s ()
use (Resource n) = liftIO $ printf "using %d\n" n

misuse :: Resource s -> Claim s ()
misuse _ = throwM UseFailed
