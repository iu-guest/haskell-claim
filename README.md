# Rationale #

Resource management in Haskell is usually performed with 'bracket'-like
functions, ensuring that deallocation actions will be performed, no
matter what exceptions are thrown.

While you can be sure, that resource is not leaked, 'braket'-like
functions permit resource handle to escape their scope, introducing
another problem -- use of resource after it was released. Here is
simple example.

```
import System.IO

main :: IO ()
main = do
  h <- withFile "output.txt" WriteMode pure
  hPutStrLn h "Hello!"
```

This program compiles, but throws at runtime following exception:

	malicious: output.txt: hPutStr: illegal operation (handle is closed)

Proably, Python or C hackers would say: "just don't do it", Haskell
way it to design interface that eliminate possibility of such errors.
Enter ``claim`` package.

# Using claim package #

Package ``claim`` derives inspiration from ``Control.Monad.ST``, which
already solved similar issue. Unfortunately, it means that redesigning
your API to use ``claim`` package is necessary backward-incompatible
change.

Let us imagine, that your library provided following interface:

````
data Resource
useResource :: Resource -> IO Int
withResource :: (MonadIO m) => String -> (Resource -> m a) -> m a
withResource str = bracket (acquireResource str) releaseResource
-- not exported
acquireResource :: (MonadIO m) => String -> m Resource
releaseResource :: (MonadIO m) => Resource -> m ()

````

## New interface ##

Usage of such interface is oblivious, but is vulnerable to error
described in rationale. Here is how interface will look after we
change it to use ``claim`` package:

````
data Resource s
useResource :: (MonadIO m) => Resource s -> ClaimT s m Int
resource :: (MonadIO m) => String -> ClaimT s m (Resource s)
resource str = claim (acquireResource str) releaseResource
--- not exported
acquireResource :: (MonadIO m) => String -> m (Resource s)
releaseResource :: (MonadIO m) => Resource s -> m ()
````

It definitely looks intimidating, so let us take it slowly.  First
change is that ``Resource`` datatype now have extra phantom type
parameter. It is required for Rank2Types magic, that is discussed
below.

Next, ``useResource`` now returns value in ``ClaimT`` monad. It
requires minimal changes, since ``ClaimT`` is instance of *MonadTrans*
(from mtl package), as such you have function ```lift :: m a -> ClaimT s m a``.

Only types of ``acquireResource`` and ``releaseResource`` function
change sligtly, since type (err, kind) of ``Resource`` changed;
implementation is likely to stay as is.

The most interesting part is that function ``withResource`` is
replaced with ``resource``, which returns resource in monad
``ClaimT``. It is implementation is same, with ``bracket`` function
replaced with ``claim``.

## Using new interface ##

Since ``ClaimT`` is monad, usage is rather straightforward, as can be
seen in followin stupid example:

````
frobnicate :: (MonadIO m) => String -> m Int
frobnicate str = runClaimT $ do
	r1 <- resource str
	r2 <- resource (reverse str)
	n1 <- useResource r1
	n2 <- useResource r2
	if n1 > n2 then pure (n1 + n2) else pure (n1 * n2)
````

The only thing, that worth of attention here is function

	runClaimT :: (forall s. ClaimT s m a) -> m a

You do not need to think too much about this fearsome type signature,
just know, that ``runClaimT`` will allow you to get value in
underlying monad, unless you are trying to smuggle resource out, as
discussed in following section.

## What happens in ClaimT, stays in ClaimT

But let us imagine, that you are feeling hackish, and trying to push
the limits, like this:

````
monster :: IO ()
monster = do
  evil <- runClaimT $ do
	r <- resource
	pure r
  -- use innocent resource for your monsterous purposes
  pure ()
````

GHC will not allow such things. Not that error message is very
user-friendly, but if you learn to search for *would escape its scope*
string, you know the way.

    • Couldn't match type ‘a0’ with ‘R.Resource s’
        because type variable ‘s’ would escape its scope
      This (rigid, skolem) type variable is bound by
        a type expected by the context:
          ClaimT s IO a0
        at smuggle.hs:(6,11)-(8,10)
      Expected type: ClaimT s IO a0
        Actual type: ClaimT s IO (R.Resource s)

At every attemt to smuggle resource or closure, referencing to the
resource over ``runClaimT`` border, GHC will complain about type
variable escaping its scope. If you still manage to smuggle resource,
please file a bug report.

# Compraison with other packages #

As far as I know, the only package that deals with resource
acquisition and release is ``resourcet``. As well described in its
documentation, its purpose is greater flexiblity about resource
releasing. By design, ``resourcet`` allows resource to be releases
prematurely, as such allowing usage of already released resource
possible.

[resourcet] http://www.stackage.org/package/resourcet
