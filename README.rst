Resource management in Haskell is usually performed with
'bracket'-like functions, ensuring that deallocation actions will be
performed, no matter what exceptions are thrown.

This pattern was abstracted into Monad by Gabriel Gonzalez in his
[blog post]_ and implemented by Michael Snoyman in his [resourcet]_
package.

Unfortunately, neither of those approaches stop programmer from using
already freed resource. Here is simple example:

.. code-block:: haskell

   import System.IO

   main :: IO ()
   main = do
     h <- withFile "output.txt" WriteMode pure
     hPutStrLn h "Hello!"

This package allows you to design interface, eliminating possibility
of such errors.

.. [blog post] http://www.haskellforall.com/2013/06/the-resource-applicative.html)
.. [resourcet] http://www.stackage.org/package/resourcet
