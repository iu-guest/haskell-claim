{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE UndecidableInstances  #-}
{-|
Module        : Control.Monad.Claim
Description   : deterministic freeing of scarce resources
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : GHC

-}

module Control.Monad.Claim (ClaimT, Claim, claim, runClaimT) where
import           Control.Monad
import           Control.Monad.Catch
import           Control.Monad.IO.Class
import           Control.Monad.Reader.Class
import           Control.Monad.State.Class
import           Control.Monad.Trans.Class
import           Control.Monad.Writer.Class

data Ops m = Ops {
  restore_     :: forall b. m b -> m b,
  finally_     :: forall a b. m a -> m b -> m a,
  onException_ :: forall a b. m a -> m b -> m a
  }

data Allocated m a = Allocated a (m ())
  deriving Functor

-- | Monad, keeping track of acquired resources and actions, required
-- to release them. To create value in this monad with associated
-- release action, use 'claim' function. Use 'lift' to inject action
-- of base monad into 'ClaimT', and 'pure' to inject pure value.
--
-- To actually execute actions in underlying monad, use 'runClaimT'.
-- Resources will be released in order, reverse to order of
-- acquisition.
data ClaimT s m a = ClaimT (Ops m -> m (Allocated m a))

-- | Particular case of 'ClaimT' with 'IO' base monad. This is little
-- violation of mtl naming convention, since there is no point in
-- using 'ClaimT' with 'Identity' as base monad.
type Claim s a = ClaimT s IO a

instance (Functor m) => Functor (ClaimT s m) where
  fmap f (ClaimT g) =
    ClaimT $ \ops -> fmap (fmap f) (g ops)

instance (Monad m) => Applicative (ClaimT s m) where
  pure x = ClaimT $ \_ -> pure $! Allocated x (pure ())
  (<*>) = ap

-- Courtesy of Data/Acquire/Internal.hs from resourect package.
instance (Monad m) => Monad (ClaimT s m) where
  ClaimT f >>= g' = ClaimT $ \ops -> do
    Allocated x free1 <- f ops
    let ClaimT g = g' x
    Allocated y free2 <- onException_ ops (g ops) free1
    return $! Allocated y (finally_ ops free2 free1)

instance (MonadIO m) => MonadIO (ClaimT s m) where
  liftIO f = ClaimT $ \ops -> do
    x <- restore_ ops (liftIO f)
    return $! Allocated x (pure ())

instance MonadTrans (ClaimT s) where
  lift act = ClaimT $ \_ -> do
    x <- act
    pure $! Allocated x (pure ())

instance (MonadReader q m) => MonadReader q (ClaimT s m) where
  ask = lift ask
  local f (ClaimT g) = ClaimT $ \ops -> local f (g ops)

instance (MonadState q m) => MonadState q (ClaimT s m) where
  state = lift . state

instance (MonadWriter w m) => MonadWriter w (ClaimT s m) where
  tell = lift . tell
  listen (ClaimT g) = ClaimT $ \ops -> do
    (Allocated x free, w) <- listen (g ops)
    pure $! Allocated (x, w) free
  pass (ClaimT g) = ClaimT $ \ops -> do
    Allocated (x, fn) free <- g ops
    pass $ pure (Allocated x free, fn)

instance (MonadThrow m) => MonadThrow (ClaimT s m) where
  throwM e = ClaimT $ \_ -> throwM e

-- | Register resource in 'ClaimT' monad. Type argument t is type of
-- resource being claimed. Those strange /forall/ in function
-- signature are just part of trickery to avoid resource escaping
-- its designated scope. For example,
--
-- @
--
-- data Resource s
-- getResource :: IO (Resource s)
-- freeresource :: Resource s -> IO ()
--
-- resource :: ClaimT s IO (Resource s)
-- resource = claim getResource freeResource
--
-- @
--
-- It is important, that resource type has extra phony type argument.
claim :: (Functor m)
        => (forall s0. m (t s0)) -- ^acquire function
        -> (forall s1. (t s1 -> m ())) -- ^release function
        -> ClaimT s m (t s)
claim create free = ClaimT $ \_ -> allocate <$> create where
  allocate v = Allocated v (free v)

-- | Run 'ClaimT' monad, freeing allocated resources.
runClaimT :: (MonadMask m)
          => (forall s. ClaimT s m a) -> m a
runClaimT (ClaimT g) = mask $ \restore -> do
  let ops = Ops restore finally onException
  Allocated x free <- g ops
  free
  pure x
